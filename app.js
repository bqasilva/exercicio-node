const express = require ('express');
const bodyParser = require('body-parser');
const app = express ();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

app.listen(port, ()=>{
    console.log("Projeto executando na porta: " + port);
});

/*Parâmetro Clientes*/
app.get ('/clientes', (req,res)=>{
    let source = req.query;
    let ret = "Nome: " + source.nome;
    res.send(`{message: ${ret}}`);
});

app.post('/clientes',(req, res)=>{
    let dados = req.body;
    let headers = req.headers["access"];
    let pass = "9999";
    let msg
    if(headers == pass){
        msg = 'acesso concedido para próxima etapa - Nome do Cliente: ';
        res.send(`{message: ${msg} ${dados.nome}}`);
    }else{
        msg = 'Acesso Negado, Verifique a senha de acesso!';
        res.send(`{message: ${msg}}`);
    }
});

/*-------------------------------------------------------*/
/*Parâmetro Funcionarios*/
app.get ('/funcionarios', (req,res)=>{
    let source = req.query;
    let ret = "Funcionario: " + source.funcionario;
    res.send(`{message: ${ret}}`);
});

app.delete('/funcionarios/:valor',(req, res)=>{
    let dados = req.params.valor;
    let headers = req.headers["access"];
    let pass = "55555";
    let msg;
    if(headers == pass){
        msg = 'acesso concedido para próxima etapa - Valor: ';
        res.send(`{message: ${msg} ${dados}}`);
    }else{
        msg = 'Acesso Negado, Verifique a senha de acesso!';
        res.send(`{message: ${msg}}`);
    }
});

app.put('/funcionarios',(req,res)=>{
    let dados = req.body;
    let headers = req.headers["access"];
    let pass = "111111";
    let msg
    if(headers == pass){
        msg = 'acesso concedido para próxima etapa - Nome do Funcionário: ';
        res.send(`{message: ${msg} ${dados.nome} }`);
    }else{
        msg = 'Acesso Negado, Verifique a senha de acesso!';
        res.send(`{message: ${msg}}`);
    }
});